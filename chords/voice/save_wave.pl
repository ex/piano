##==============================================================================
## Saves JSON audio response from Google as a WAV file (from the clipboard)
## https://cloud.google.com/text-to-speech/
## https://stackoverflow.com/questions/51687462/firefox-developer-tools-truncates-long-network-response-chrome-does-not-show
## cpan> force install Win32::GUI
## Author: Laurens Rodriguez.
## This file is in the public domain
##------------------------------------------------------------------------------
use strict;
use warnings;
use Win32;
use Win32::Clipboard;
use Win32::GUI;
use Data::Dumper;
use MIME::Base64;
use JSON;

my $clipboard = Win32::Clipboard()->Get();
my $json = decode_json( $clipboard );

# print Dumper($json);
# print $json->{"audioContent"};
my $audioData = decode_base64( $json->{'audioContent'} );

my $filename = 'audio';
my $Dialog = Win32::GUI::DialogBox->new( -name  => "Dialog",
                                         -title => "Enter file name",
                                         -pos   => [ 300, 300 ],
                                         -size  => [ 220, 90 ] );

$Dialog->AddButton( -name => "ButtonOk",
                    -text => "Ok",
                    -pos  => [ 170, 20 ],
                    -ok => 1 );

$Dialog->AddTextfield( -name    => "TextField",
                       -left    =>  10,
                       -top     =>  20,
                       -width   => 150,
                       -height  => 20 );

$Dialog->Show();
$Dialog->TextField->SetFocus();
Win32::GUI::Dialog();
undef $Dialog;

print "Saved file: $filename.wav\n";
open( my $fh, '>:raw', "$filename.wav" ) or die "Could not open file '$filename.wav' $!";
    print $fh $audioData;
close $fh;

exit(0);

##------------------------------------------------------------------------------
sub Dialog_Terminate
{
    return -1;
}

sub ButtonOk_Click
{
    $filename = $Dialog->TextField->Text();
    $|=1;
    return -1;
}
